---
title: api.navi.cc API v1
---

# API v1

Это официальный документ для api.navi.cc версии 1.0. Если вы испытываете проблемы или имеете пожелания, пожалуйста свяжитесь с нами по адресу [support](mailto:baden.i.ua@gmail.com?subject=api.navi.cc_APIv1).

* TOC
{:toc}

## Схема

Все запросы начинаются с префикса `http://api.newgps.navi.cc/1.0`. Далее следует путь к ресурсу. Например, получить события, относящиеся к системе с ключом :skey можно по адресу `/systems/:skey/logs`. Полный путь будет иметь вид:

<pre class="terminal">
$ curl -i http://api.newgps.navi.cc/1.0/systems/KEYFORSOMESYSTEM/logs

HTTP/1.1 200 OK
Server: nginx
Date: Fri, 12 Oct 2012 23:33:14 GMT
Content-Type: application/json; charset=utf-8
Connection: keep-alive
Status: 200 OK
ETag: "a00049ba79152d03380c34652f2cb612"
Content-Length: 5
Cache-Control: max-age=0, private, must-revalidate
Access-Control-Allow-Credentials: true
Access-Control-Allow-Origin: *

[]
</pre>

В большинстве запросов должен быть указан домен, с которого осуществляется запрос (CORS). Для этого должен быть установлен **Origin** в заголовке запроса.

<pre class="terminal">
$ curl -i http://api.newgps.navi.cc/1.0/info -H "Origin: http://some-site.com"
</pre>

В данный момент разрешены запросы с любого домена `Access-Control-Allow-Origin: *`, но нужно иметь ввиду, что в дальнейшем это может быть изменено.

Передаваемые и получаемые данные имеют JSON-формат.

## Параметры

Основные параметры являются частью пути запроса, далее запись вида `:value` подразумевает, что вместо `:value` необходимо подставить основной параметр, например в запросе `DELETE http://api.newgps.navi.cc/1.0/account/systems/:skey` вместо `:skey` необходимо указать ключ системы.

Большинство API-запросов могут содержать дополнительные параметры.

Для GET запросов, любые параметры, не являющиеся частью пути могут быть заданы в запросе:

<pre class="terminal">
$ curl -i http://api.newgps.navi.cc/1.0/info?verbose=yes&state=all
</pre>

Для POST-запроса, дополнительные параметры задаются в теле запроса. Если тело POST-запроса задано в JSON-формате, то должен быть установлен заголовок `Content-Type: application/json; charset=utf-8`.

<pre class="terminal">
$ curl -i -d '{"username":"baden","password":"333"}' http://api.newgps.navi.cc/1.0/login -H "Content-type: application/json; charset=UTF-8" -H "Origin: http://some-site.com"
</pre>

Если запрос содержит только строковые параметры, то в качестве альтернативы может использоватьcя `Content-Type: application/x-www-form-urlencoded`, но предпочтение отдается `application/json`:

<pre class="terminal">
$ curl -i -X POST http://api.newgps.navi.cc/1.0/auth -d "grant_type=password&username=baden&password=123" -H "Origin: http://some-site.com"
</pre>

## Ошибки

Если была попытка выполнения неавторизованного запроса, то будет возвращена ошибка:

        HTTP/1.1 401 Unauthorized
        Access-Control-Allow-Headers: content-type, if-modified-since, authorization, x-requested-with
        Access-Control-Allow-Credentials: true
        Access-Control-Allow-Origin: *
        www-authenticate: Bearer
        Content-Length: 0

Пример возвращаемой ошибки при ошибке в параметрах:

        HTTP/1.1 400 Bad Request
        Content-Length: 69
        Content-Type: application/json; charset=utf-8

        {
                "status_code": 400,
                "message": "Problems parsing JSON"
        }

## HTTP-Методы

Там где это возможно, используются соответствующие HTTP-методы:

HEAD
: Получить только информацию заголовка HTTP.

GET
: Получение ресурса

DELETE
: Удаление ресурса

POST
: Создание ресурса или выполнение специальных действия над ресурсом.

PUT
: Замена ресурса

PATCH
: Обновления ресурса частичными данными из JSON-запроса.

## Авторизация

Для большинства запросов требуется авторизация. После выполнения запроса `/auth` будет возвращен ключ авторизации access_token.

Доступны три способа авторизации запроса:

### Указание access_token как параметра запроса.

<pre class="terminal">
$ curl -i "http://api.newgps.navi.cc/1.0/account?access_token=BUtSqeNhjASqlqgakvkzroQRKxt65yum" -H "Origin: http://some-site.com"
</pre>

### Указание access_token в заголовке Authorization.

<pre class="terminal">
$ curl -i "http://api.newgps.navi.cc/1.0/account" -H "Origin: http://some-site.com" -H "Authorization: Bearer BUtSqeNhjASqlqgakvkzroQRKxt65yum"
</pre>

### Установка cookies.

Для запросов, поддерживающих `Access-Control-Allow-Credentials: true` будут установлены cookies.

## User Agent

Все запросы для корректной работы требуют чтобы был установлен `User Agent`.

## Формат основных ресурсов

### Базовые типы

string
: Строковое значение

number
: Числовое значение, в том числе значение с плавающей точкой.

integer
: Целочисленное числовое значение

boolean
: Булевое значение истина/ложь (true/false)

object
: Значение типа объект.

array
: Список значений. Список как правило содержит элементы одного типа, хотя это и не обязательно.
  Тип элементов указывается как **attay of `тип`**.

null
: Пустое значение. Применяется в случаях, когда необходимо чтобы поле существовало,
  но запись не содержала значения.

### Специальные типы

datetime
: Значение типа **string**. Метка времени в формате ISO 8601: "YYYY-MM-DDTHH:MM:SSZ"

dt
: Значение типа **integer**. Метка времени для данных, требующих особых условий хранения или передачи.
  Целочисленное значение. Дата/время указывается в Unux-time формате.
  Количество секунд, прошедших с полуночи (00:00:00 UTC) 1 января 1970 года (четверг).

  Для примера такое значение в ЯП Javascript может быть использовано следующим образом:

  <pre class="highlight"><code class="language-javascript">
    new Date(1372700873 * 1000)
    // => Mon Jul 01 2013 13:47:53 GMT-0400 (EDT)
  </code></pre>

key
: Значение типа **string**. Ключ ресурса. Например, список наблюдаемых систем представляет собой
  запись типа **array of key**.

any
: Значение может принимать любой тип из приведенных выше.



## Cross Origin Resource Sharing

API поддерживает Cross Origin Resource Sharing (CORS) для AJAX запросов.
Смотри [CORS W3C working draft](http://www.w3.org/TR/cors), или
[this intro](http://code.google.com/p/html5security/wiki/CrossOriginRequestSecurity).

Пример OPTIONS запроса через браузет с сайта `http://some-site.com`:

<pre class="terminal">
$ curl -i http://api.newgps.navi.cc/1.0/account -H "Origin: http://some-site.com" -X OPTIONS

HTTP/1.1 204 No Content
Access-Control-Allow-Origin: http://some-site.com
Access-Control-Allow-Credentials: true
Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE
Access-Control-Allow-Headers: content-type, if-modified-since, authorization, x-requested-with
...
</pre>

На данный момент поддерживаются запросы с любого домена.

