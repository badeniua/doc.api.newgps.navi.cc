---
title: Группы пользователей | api.navi.cc
---

# Группы пользователей

* TOC
{:toc}

Чтобы иметь доступ к административным ресурсам, необходимо чтобы авторизованый пользователь состоял в группе `admin`.

## Список групп

    GET /admin/groups

### Ответ

<%= headers 200 %>
<%= json [
        {
            :url => 'http://api.newgps.navi.cc/1.0/admin/groups/groupadminid',
            :id => 'groupadminid',
            :groupname => 'admin'
        },
        {
            :url => 'http://api.newgps.navi.cc/1.0/admin/groups/groupmssid',
            :id => 'groupmssid',
            :groupname => 'MSS Ukraine'
        }
    ]
%>


## Получить группу

    GET /admin/groups/:id

### Ответ

<%= headers 200 %>
<%= json \
    :url => 'http://api.newgps.navi.cc/1.0/admin/groups/mygroupid',
    :id => 'mygroupid',
    :groupname => 'My company name',
    :permission => 'regular',
    :members_count => 3
%>


## Создать группу

    POST /admin/groups

### Запрос

groupname
: _Обязательный_ **string** - Имя группы пользователей

grouppassword
: _Обязательный_ **string** - Проверочное слово для вступления в группу пользователей

permission
: _Необязательный_ **string** - Полномочия группы

    `regular` - Обычная группа пользователей. **Значение по-умолчанию**.

    `service` - Группа поддержки пользователей. Члены данной группы имеет некоторые дополнительные полномочия.

<%= json \
    :groupname => 'mycompany',
    :grouppassword => '1234'
%>

### Ответ

<%= headers 201 %>
<%= json \
    :groupname => 'mycompany',
    :id => 'mycompanygroupid',
    :permission => 'regular',
    :members_count => 0
%>


## Редактировать группу

    PATCH /admin/groups/:id

### Запрос

groupname
: _Необязательный_ **string** - Имя группы пользователей

grouppassword
: _Необязательный_ **string** - Проверочное слово для вступления в группу пользователей

permission
: _Необязательный_ **string** - Полномочия группы


### Ответ

<%= headers 200 %>
<%= json \
    :id => 'mygroupid',
    :groupname => 'My company\'s new name'
%>

## Удалить группу

    DELETE /admin/groups/:id

### Ответ

<%= headers 204 %>

## Получить список членов группы

    GET /admin/groups/:id/accounts

### Ответ

<%= headers 200 %>
<%= json \
    [
        {
            :url => 'http://api.newgps.navi.cc/1.0/users/user1',
            :id => 'user1',
            :username => 'baden',
            :title    => 'Денис',
            :email    => 'denis@batrak.com',
            :company  => 'MSS',
            :location => 'Днепродзержинск'
        },
        {
            :url => 'http://api.newgps.navi.cc/1.0/users/user2',
            :id => 'user2',
            :username => 'alex',
            :title    => 'Денис',
            :email    => '',
            :company  => 'MSS',
            :location => 'Днепропетровск'
        }
    ]
%>

## Проверить членство пользователя

    GET /admin/groups/:id/accounts/:user

### Ответ если пользователь является членом группы

<%= headers 204 %>

### Ответ если пользователь не является членом группы

<%= headers 404 %>

## Добавить пользователя в группу

    PUT /admin/groups/:id/accounts/:user

### Ответ

<%= headers 204 %>

## Удалить пользователя из группы

    DELETE /admin/groups/:id/accounts/:user

### Ответ

<%= headers 204 %>
