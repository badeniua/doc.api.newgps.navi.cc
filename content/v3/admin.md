---
title: Admin | api.navi.cc
---

# Администрирование

* TOC
{:toc}

Для управления работой сервиса введена специальная группа администраторов `admin`.
Чтобы иметь доступ к административным ресурсам, необходимо чтобы авторизованый пользователь состоял в данной группе.

