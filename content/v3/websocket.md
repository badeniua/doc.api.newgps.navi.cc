---
title: Обновление ресурсов | api.navi.cc
---

# Механизм уведомления об обновлении ресурсов

* TOC
{:toc}

Каждый WEB-клиент может создать дуплексное подключение для уведомления об изменении ресурсов сервера.
При изменение какого-либо ресурса (или нескольких) происходит отправка сообщения:

<%= json \
    "messages" => [{
        "resource" => "system",
        "id" => "MDEzMjI2MDAwMTk4MjE0",
        "data" => {
            "title" => "Новое наименование ТС"
        }
    }, {
        "resource" => "system",
        "id" => "MDEzMjI2MDAwMTk4MjE0",
        "data" => {
            "dymanic" => {
                "lastping" => "1378030209",
                "fsource" => "7",
                "sats" => "6",
                "vout" => "12.99",
                "latitude" => "48.501295",
                "longitude" => "34.62528",
                "speed" => "0.16668",
                "vin" => "3.81",
                "course" => "314",
                "flags" => "0",
                "fuel" => "128",
                "dt" => "1378030200",
                "csq" => "24"
            }
        }
    }, {
        "resource" => "acount",
        "id" => "MDEzMjI2MDAwMTk4",
        "data" => {
            "title" => "Мое новое имя"
        }
    }]

%>

Чтобы получать уведомление об изменении ресурса, клиент должен подписаться на уведомления
об изменении одного или нескольких ресурсов:

<%= json \
    "subscribe" => [{
        "resource"  => "system",
        "id"        => "MDEzMjI2MDAwMTk4MjE0"
    }, {
        "resource"  => "system",
        "id"        => "MDFzWjI2MDBrMTk4MjW0"
    }]
%>

Если получение уведомления по определенному ресурсу более не нужно, то необходимо отписаться:

<%= json \
    "unsubscribe" => [{
        "resource"  => "system",
        "id"        => "MDEzMjI2MDAwMTk4MjE0"
    }]
%>

Если поле `data` имеет значение **null**, то это значит что сервер не может определить какого рода измения
произошли с ресурсом и клиенту требуется самостоятельно запросить ресурс заново.

<%= json \
    "messages" => [{
        "resource" => "account",
        "id" => "baden",
        "data" => nil
    }]

%>