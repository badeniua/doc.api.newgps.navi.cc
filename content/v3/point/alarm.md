---
title: Сбор данных с трекеров | point.navi.cc
---

# Работа с тревожной кнопкой

* TOC
{:toc}

## Подтверждение получение сигнала тревоги

Если на какой-либо запрос трекера сервер, перед обычным ответом, отвечает строкой `ALARM_CONFIRM`,
то трекер подтверждает получение сигнала тревоги оператором и отвечает:

    GET /inform/del&msg=ALARM_CONFIRM


### Ответ

<%= headers 200, "Content-Type" => "application/octet-stream" %>
<pre class="codeline">
INFORM: OK
</pre>


## Отмена сигнала тревоги

Если на какой-либо запрос трекера сервер, перед обычным ответом, отвечает строкой `ALARM_CANCEL`
то трекер подтверждает отмену тревоги оператором и отвечает:

    GET /inform/del&msg=ALARM_CANCEL

### Ответ

<%= headers 200, "Content-Type" => "application/octet-stream" %>
<pre class="codeline">
INFORM: OK
</pre>
