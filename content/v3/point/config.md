---
title: Сбор данных с трекеров | point.navi.cc
---

# Конфигурация параметров трекера

* TOC
{:toc}



## Сохранение конфигурации

    POST /config?imei=:imei

### Запрос

Формат тела запроса:

    Имя_параметра тип_параметра значение_параметра заводское_значение

Значения разделяются символами пробела `0x20` или табуляции `0x09`.

Передача завершается строкой `END`.

<pre class="codeline">
gps.T1.1    INT 600 600
gps.T1.0    INT 30  30
gps.B1.3    INT 512 512
gps.B1.2    INT 512 512
…
…
…
gps.T0.2    INT 10  10
gps.T0.3    INT 10  10
akkum.U.1   INT 907 907
akkum.U.0   INT 862 862
akkum.U.3   INT 984 984
akkum.U.2   INT 911 911
akkum.U.4   INT 911 911
gps.V0.2    INT 10  10
gps.V0.3    INT 20  20
gps.V0.0    INT 5   5
gps.V0.1    INT 20  20
gps.T4.0    INT 720 720
gps.T4.1    INT 240 240
gps.T4.2    INT 720 720
END
</pre>

Поддерживаемые типы параметров:

| Значение | Описание
|:----:|:-----
| INT  | Числовое значение в диапазоне -32768...32767
| LONG | Числовое значение в диапазоне -2<sup>31</sup>...2<sup>31</sup>-1
| STR16 | Строковое значение, длиной не более 16ти символов
| STR32 | Строковое значение, длиной не более 31го символов

### Ответ

<%= headers 200, "Content-Type" => "application/octet-stream" %>
<pre class="codeline">
CONFIG: OK
</pre>

## Обновление конфигурации

Если на какой-либо запрос трекера сервер вернул [дополнительную команду](/v3/point/#section-1) `CONFIGUP`, то трекер запрашивает обновление конфигурации

    GET /params?imei=:imei&cmd=params

### Ответ

Ответа имеет следующий формат

    PARAM параметр значение

Завершает список изменяемых параметров строка `FINISH`.

<%= headers 200, "Content-Type" => "application/octet-stream" %>
<pre class="codeline">
PARAM gps.V0.1 15
PARAM akkum.U.1 560
FINISH
</pre>

Трекер подтверждает изменение параметров запросом:

    GET /params?imei=:imei&cmd=confirm

### Ответ

<%= headers 200, "Content-Type" => "application/octet-stream" %>
<pre class="codeline">
CONFIRM
</pre>
