---
title: Системы | api.navi.cc
---

# Параметры трекеров

* TOC
{:toc}

## Получение программируемых параметров трекера

    GET /params/:key

### Ответ

<%= headers 200 %>
<%= json \
    "id" => "MDEzMjI2MDAwMTk4MjE0",
    "data" => {
        "gps.VIGNACC" => {
            "default" => "4000",
            "type" => "INT",
            "value" => "4000"
        },
        "akkum.block.vbat" => {
            "default" => "1092",
            "type" => "INT",
            "value" => "1092"
        },
        "gps.TF.MOVE" => {
            "default" => "60",
            "type" => "INT",
            "value" => "60"
        },
        "gsm.alarm" => {
            "default" => "",
            "type" => "STR16",
            "value" => ""
        }
    },
    "queue" => {
        "gps.TF.MOVE" => "30",
        "gsm.alarm" => "+380679332330"
    }
%>

## Изменение программируемого параметра трекера.

Изменение происходит в два этапа.
Сначала устанавливается значение поля `queue` в значение, на которое необходимо изменить параметр.
Когда трекер получит задание на изменение и подтвердит изменение, значение из поля `queue` попадет
в поле `value`. В обоих случаях происходит обновление ресурса `params`.

    PATCH /params/:id

### Запрос

<%= json \
    "queue" => {
        "gps.TF.MOVE" => "70",
        "gps.FAIL" => "6"
    }
%>

TODO! C PATCH оказывается не все гладко, мало какие библиотеки его поддерживают. Необходимо переделать на другой формат:

    POST /params/:id/queue

### Запрос

<%= json \
    "gps.TF.MOVE" => "70",
    "gps.FAIL" => "6"
%>

### Ответ

<%= headers 204 %>

## Отмена изменений.

Удалить очередь на изменение значений:

    DELETE /params/:id/queue


    POST /params/:id/queue
    {}


    PATCH /params/:id

<%= json \
    "queue" => {
    }
%>

### Ответ

<%= headers 204 %>

