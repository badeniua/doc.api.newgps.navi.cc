# Авторизация

* TOC
{:toc}

## Авторизация в системе

    POST /auth

### Запрос

grant_type
: _Обязательный_ **string** - На данный момент должен иметь значение "**password**".

username
: _Обязательный_ **string** - Имя пользователя

password
: _Обязательный_ **string** - Пароль

scope
: _Необязательный_ **string** - TBD

### Ответ

<%= headers 200 %>
<%= json :auth %>

access_token
: _Обязательный_ **string** - Ключ


### Пример

<pre class="terminal">
$ curl -i -X POST "http://api.newgps.navi.cc/1.0/auth" -H "Origin: http://some-site.com" -d "grant_type=password&username=baden&password=123"

HTTP/1.1 200 OK
connection: keep-alive
server: Cowboy
date: Tue, 10 Sep 2013 09:07:05 GMT
content-length: 167
content-type: text/html

{
    "token_type": "bearer",
    "scope": "",
    "resource_owner": "{user,baden}",
    "expires_in": "3600",
    "access_token": "szKt3ToQ6mFHlsCgmbC3eJtzdSZP2LLk"
}
</pre>



## Создание пользователя

    POST /register

### Запрос

username
: _Обязательный_ **string** - Имя пользователя

password
: _Обязательный_ **string** - Пароль

email
: _Необязательный_ **string** - Электронный ящик для процедуры восстановления пароля

title
: _Необязательный_ **string** - Отображаемое имя

groupname
: _Необязательный_ **string** - Наименование группы пользователей.

grouppassword
: _Необязательный_/_Обязательный_ **string** - Проверочное слово для вступление в группу пользователей. Параметр _обязательный_ если задано поле **groupname**.

newgroup
: _Необязательный_/_Обязательный_ **boolean** - Если установлен в значение `true`, то дополнительно необходимо создать группу с именем groupname. Параметр _обязательный_ если задано поле **groupname**.


### Ответ


<%= headers 200 %>
<%= json (:register) %>

Если пользователь с указанным именем уже существует, то будет возвращен ответ:

<%= headers 409 %>
<%= json \
    :message => "Validation Failed",
    :errors => [{
      :code     => "exist",
      :field    => :user,
      :resource => :GroupMember
    }]
%>

Если установлен `newgroup` и группа с указанным наименованием уже существует, то будет возвращен ответ:

<%= headers 409 %>
<%= json \
    :message => "Validation Failed",
    :errors => [{
      :code     => "exist",
      :field    => :groupname,
      :resource => :Group
    }]
%>

Если не установлен `newgroup` и группа с указанным наименованием не существует, то будет возвращен ответ:

<%= headers 404 %>
<%= json \
    :message => "Validation Failed",
    :errors => [{
      :code     => "notfound",
      :field    => :groupname,
      :resource => :Group
    }]
%>

Если не установлен `newgroup` и не совпало проверочное слово для группы, то будет возвращен ответ:

<%= headers 409 %>
<%= json \
    :message => "Validation Failed",
    :errors => [{
      :code     => "wrongpassword",
      :field    => :grouppassword,
      :resource => :Group
    }]
%>

## Выход из акаунта

Можно использовать если требуется сброс coockies.

    GET /logout

### Ответ

<%= headers 200 %>
<%= json :result => "logout" %>

