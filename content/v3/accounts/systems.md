---
title: Системы пользователя | api.navi.cc
---

# Системы авторизованного пользователя

* TOC
{:toc}

## Получение всех наблюдаемых систем

    GET /systems
    GET /account/systems

### Ответ

<%= headers 200 %>
<%= json :systems %>

## Добавление системы в список наблюдения авторизованного пользователя

    PUT /account/systems/:imei

### Ответ

<%= headers 201, :Location => "http://api.navi.cc/account/systems/*" %>
<%= json :systems %>

## Добавление одной или нескольких систем в список наблюдения авторизованного пользователя

    POST /account/systems

### Запрос

<%= json \
    :cmd    => "add",
    :imeis  => ["IMEI1", "IMEI2", "IMEI3"]
%>

### Ответ

Будет возвращен массив, каждый элемент которого является объектом одного из типов:




## Изменение порядка наблюдаемых систем

    PATCH /account

### Запрос

<%= json \
    :skeys => ["SKEY3", "SKEY2", "SKEY1"]
%>

Обязательно в списке систем должны быть все те идентификаторы, которые были до этого

### Ответ

<%= headers 200 %>
<%= json :skeys => ["SKEY3", "SKEY2", "SKEY1"] %>

## Удаление системы из списка наблюдения

    DELETE /account/systems/:skey

### Ответ

<%= headers 204 %>
