---
title: api.navi.cc API
---

# api.navi.cc API

Это официальный документ для api.navi.cc версии 1.0. Если вы испытываете проблемы или имеете пожелания, пожалуйста свяжитесь с нами по адресу [support](mailto:baden.i.ua@gmail.com?subject=api.navi.cc_APIv1).

Навигация по ресурсам API справа >>

#### Реализованные ресурсы

* `[✓]` <del>Аккаунт.</del>
* `[✓]` <del>Системы</del>
* `[ ]` Настройки
* `[ ]` События
* `[ ]` Треки
* `[ ]` Гео-зоны
* `[ ]` Маршруты
* `[ ]` Отчеты

